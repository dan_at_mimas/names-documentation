<html><meta charset="UTF-8"><style>html { 
    font-size: 100%; 
    overflow-y: scroll; 
    -webkit-text-size-adjust: 100%; 
    -ms-text-size-adjust: 100%; 
}

body{
    font-family: helvetica, arial, freesans, clean, sans-serif;
    color: #333;
    background-color: #fff;
    border-color: #999999;
    border-width: 2px;
    line-height: 1.5;
    margin: 2em 3em;
    text-align:left;
    padding: 0 100px 0 100px;
}

pre{
    background-color: #eee;
    padding: 10px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    overflow: auto;
}
code{
    background-color: #eee;
    padding: 1px 3px;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px; 
}
pre code {
    padding-left: 0px;
    padding-right: 0px;
}
li p{
    margin: 0.3em;
}
ul > li{
    list-style-type: disc;
}
a:link, a:visited{
    color: #33e;
    text-decoration: none;
}
a:hover{
    color: #00f;
    text-shadow:1px 1px 2px #ccf;
    text-decoration:underline;
}
h1{
    color: #999;
    font-weight: 400;
    font-size: 36px;
}
h2{
    border-bottom: 1px dotted #aaa;
    margin-bottom: 1em;
    color: #333;
    font-size: 30px;
}
h3{
    color: #666;
    font-size: 24px;
}
h4 {
    font-size: 21px;
}
h5 {
    font-size: 18px;
}
.shadow{
    -webkit-box-shadow:0 5px 15px #000;
    -moz-box-shadow:0 5px 15px #000;
    box-shadow:0 5px 15px #000;     
}
</style><body><h1>Names Prototype</h1>
<p>Author:     Daniel Needham <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#97;&#110;&#105;&#101;&#108;&#46;&#110;&#101;&#101;&#100;&#104;&#97;&#109;&#64;&#109;&#97;&#110;&#99;&#104;&#101;&#115;&#116;&#101;&#114;&#46;&#97;&#99;&#46;&#117;&#107;">&#100;&#97;&#110;&#105;&#101;&#108;&#46;&#110;&#101;&#101;&#100;&#104;&#97;&#109;&#64;&#109;&#97;&#110;&#99;&#104;&#101;&#115;&#116;&#101;&#114;&#46;&#97;&#99;&#46;&#117;&#107;</a></p>
<p>Version:    1.0</p>
<h2>Overview</h2>
<p>The Names Project was originally tasked with investigating the requirements for a name authority service for UK repositories which would uniquely and persistently identify individuals and institutions active in research in this country.</p>
<p>As part of this work a prototype name authority service was developed with three key objectives:</p>
<ol>
<li>Provide disambiguation of meta data pertaining to individuals and institutions within external source data sets to attempt to identify the unique individuals within them.</li>
<li>To build a database of uniquely identified individuals and institutions, each assigned a unique identifier.</li>
<li>To provide access to these names records through a web based API, providing the associated data in a number of output formats.</li>
</ol>
<p>This document details the resulting system that was developed to achieve these goals, and each of the individual components that make up the system as a whole.</p>
<h2>High Level Architecture</h2>
<p>The Names system comprises of several conceptual components that can be used independently, integrated with other applications, or used in conjunction to replicate a name authority service as demonstrated by the Names service <a href="http://names.mimas.ac.uk">prototype</a></p>
<p>The key system components are as follows:</p>
<ol>
<li>
<p><strong>Names Disambiguator</strong></p>
<p>A package that provides functionality for normalising source meta data pertaining to individuals or institutions and then comparing each of these normalised records against each other to derive match scores. These match scores can then be used to identify candidate matches.</p>
</li>
<li>
<p><strong>Names Database Manager</strong></p>
<p>A package providing an interface between an application and an instance of the names database, allowing data to be added, retrieved and updated in a standardised way across applications.</p>
</li>
<li>
<p><strong>Names Database</strong></p>
<p>An database modelled using the Names data architecture to contain names records for individuals and institutions with all of their associated attributes.</p>
</li>
<li>
<p><strong>Data handlers</strong></p>
<p>A conceptual application that pulls in meta data pertaining to individuals or institutions, and then uses the Names Disambiguator package to compare records to identify potentially matching entities. It may then go on to match against and update an instance of the Names Database using the Names Database Manager.</p>
</li>
<li>
<p><strong>Names web application</strong></p>
<p>A web application sitting on top of a Names Database using the Names Database Manager as an interface. The application provides simple search / retrieval of records through a user interface and programmatically through an API.</p>
</li>
</ol>
<p>The following diagram gives a high level overview of how these components fit together.</p>
<p><img alt="System Architecure Diagram" src="https://docs.google.com/drawings/d/1J2pU8X4vU3-X1a_BJfNqe8xo2eZ5kmh0n-KMSWF20FE/pub?w=1086&amp;h=390" /></p>
<h2>Names Disambiguator</h2>
<p><a href="https://bitbucket.org/dan_at_mimas/names-disambiguator">https://bitbucket.org/dan_at_mimas/names-disambiguator</a></p>
<p>The Names Disambiguator package provides functionality for comparing disparate sources of information about individuals or institutions within external datasources in an effort to determine the unique entities that they describe.</p>
<p>This package is intentionally separate from the underlying data architecture of the Names prototype. It is intended only to try and provide a score for the level of likeness between a number of source records. Therefore it is entirely flexible in how it is used and for what end purpose.</p>
<p>There are three main facets to the Disambiguator package:</p>
<ol>
<li>
<p><strong>Normalisation</strong></p>
<p>In order to perform matching between different source records it is first of all necessary to normalise the meta data available pertaining to the entities so that they can easily be compared.
To that end the Disambiguator package provides a number of normalised types that source meta data can be transformed into including:</p>
<ul>
<li>NormalisedRecord - The umbrella record that all meta data is associated with.</li>
<li>NormalisedName - A name associated with the entity.</li>
<li>NormalisedAffiliation - An institution associated with the entity.</li>
<li>NormalisedIdentifier - An external identifier associated with the entity.</li>
<li>NormalisedResultPublication - A publication associated with the entity.</li>
<li>NormalisedFieldOfActivity - A field of activity or interest associated with the entity.</li>
<li>NormalisedTitle - A salution associated with the entity.</li>
<li>NormalisedCollaboration - An entity this entity has collaborated with.</li>
<li>NormalisedHomepage - A URL that uniquely associated with this entity.</li>
</ul>
<p>An application intending to use the Disambiguation package must first of all transform its source metadata into these normalised formats before matching can occur.</p>
</li>
<li>
<p><strong>Matching</strong></p>
<p>Once normalisation of source meta data has occured it is then possible to use the Disambiguator package to match the resulting list of entities. The matching process essentially compares the various bits of normalised meta data associated with an entity against those of another entity and attempts to derive a match score. This score can then be used to determine the likelyhood of a match between two entities.</p>
<p>In order to make matching more flexible for specific contexts the matching process can be passed a map of weightings which help determine how important similarity of particular types of meta data is in the overall outcome of a match.</p>
</li>
<li>
<p><strong>Merging</strong></p>
<p>After matching has occured it may be desirable to merge normalised records that are determined to have obtained a sufficent match score to be consider a match (i.e. pertaining to the same individual). Here a number of methods are provided to facilite merging of records whose match score meet a particular threshold.</p>
<p>The resulting merged list can then me used int a variety of means, for example to update an instance of the Names Database.</p>
</li>
</ol>
<h2>Names Database Manager</h2>
<p><a href="https://bitbucket.org/dan_at_mimas/names-database-manager">https://bitbucket.org/dan_at_mimas/names-database-manager</a></p>
<p>The Names Database Manager is used to manage interactions with a Names Database instance by different clients in a standardised and consistent manner to avoid introduction of data errors.</p>
<p>The Names Database Manager package firstly provides a number of data types that are essentially normalised types as described in the Names Disambiguation section of this document, with some extra attributes and methods that makes retrieving them from the database for comparison with normalised records, and any subsequent changes within the database easier.</p>
<p>For example each data type adds the following information to a normalised record:</p>
<ol>
<li>The database id</li>
<li>The source name</li>
<li>The source URL</li>
<li>The source identifier</li>
<li>The match score that was derived when the source record for this piece of data was compared aginst this one and a match was determined to have been identified.</li>
</ol>
<p>Furthermore each data type provides methods to serialise and deserialise names records attributes easily and consistently.</p>
<p>The Names Database Manager also provides methods for easily searching over a Names Database instance, as well as updating records within the database.</p>
<p>The Names Database Manager expects the underlying Names Database instance to conform to the structure described in the Names Database section of this document and detailed in Appendix a.</p>
<h3>Identifier minting</h3>
<h2>Names Database</h2>
<p>In order to store the authority records derived from the disambiguation process a database has been built, using the <a href="http://names.mimas.ac.uk/files/Names_Data_Analysis_Report_7Apr08.pdf">Data Analysis Report</a> as a guide. </p>
<p>The existing Names database was built using <a href="http://www.mysql.com/">MySQL</a> because it is free to use and well supported. There is nothing to stop any future iteration of the Names service being altered to use a different database provider, and in fact this may be desirable where scaling becomes an issue.</p>
<p>In the Names data architecture an entity (individual or institution) can have lots of attributes associated with it, and must be searchable over many of these attributes in conjunction. For performance reasons (both for insertion and retrieval) it was determined that full database normalisation would not be especially performant, especially with potentially millions of identified individuals each with hundreds of related attributes.</p>
<p>Instead entity attributes are serialised into columns in a main record table, and then summary tables are populated with the attributes that can be searched over. Though this greatly improves performance, it can make managing the database quite cumbersome, which is why use of the Names Database Manager package is desirable, as it handles serialisation and deserialisation of objects in a standardised manner.</p>
<p>For the full database structure see Appendix a.</p>
<h2>Data Handler</h2>
<p>Data handler is a term given to an application that uses the Names Disambiguation package and optionally the Names Database Manager package to disambiguate individuals or institutions within an external data source.</p>
<p>The primary functions of a data handler are:</p>
<ol>
<li>
<p><strong>Obtain external meta data</strong></p>
<p>Firstly the data handler must obtain the meta data to be used for disambiguation from whatever source deemed necessary using whatever means required to do so. The data source could be a database, and xml file, a web api etc...</p>
</li>
<li>
<p><strong>Normalise source data</strong></p>
<p>In order to perform matching it is first necessary to normalise the source meta data using the Names Disambiguator package.</p>
</li>
<li>
<p><strong>Match records</strong></p>
<p>Once normalised, the records can be matched against each other to derive match scores, which can be used to determine likely matches between entities.</p>
</li>
<li>
<p><em>Optional</em> <strong>Match against existing Names Database instance</strong></p>
<p>It may be desirable to match the normalised source records against the existing Names records within a Names Database instance. In order to do this it is necessary to obtain candidate matches from the Names Database instance (which will be returned in a normalised format) using the Names Database Manager. These existing records can then be compared against in the same way as described in point 3.</p>
</li>
<li>
<p><em>Optional</em> <strong>Add to / Update Names Database instance</strong></p>
<p>It may be desirable to use the results of matching to update an existing Names Database instance. This is done using the Names Database Manager. In the case of source records where no match was made with existing records in the Names Database instance a new record will be created. In cases where a match was found, the source record will be merged into an existing record.</p>
</li>
</ol>
<p>Examples of data handlers are given in the Misc section of this document.</p>
<h2>Names web application</h2>
<p><a href="https://bitbucket.org/dan_at_mimas/names-ui-barebones">https://bitbucket.org/dan_at_mimas/names-ui-barebones</a>
<a href="https://bitbucket.org/dan_at_mimas/names-ui-production">https://bitbucket.org/dan_at_mimas/names-ui-production</a></p>
<p>The Names UI is a RESTful web application built using the <a href="http://www.playframework.com/">Java Play Framework</a> and providing search / retrieval over a Names Database instance using the Names Database Manager package. The web application has been designed so that it can easily be recreated and branded on top of a Names instance, meaning it would be possible to create your own local name authority service.</p>
<p>Any record created within the Names database is assigned a unique identifier, which is also a resolvable URL (e.g. http://names.mimas.ac.uk/individual/1). If this URL is resolved by a client, the client will be presented with the record's associated meta data, currently:</p>
<ul>
<li>Associated names</li>
<li>External identifiers</li>
<li>Affiliations</li>
<li>Fields of activity</li>
<li>Result Publications</li>
<li>Collaborative relationships</li>
</ul>
<p>Furthermore records can be searched over by the following attributes:</p>
<ul>
<li>Name - The names associated with entities</li>
<li>Affiliation - Corporate bodies affiliated with entities</li>
<li>Field of activity - Areas of interest associated with entities</li>
<li>Identifier - An external identifier associated with an entity.</li>
</ul>
<p>Search results and individual records can be retrieved in HTML, Json &amp; RDF-XML.</p>
<p>More information about API usage, response and request formats is given in the application and in Appendix B of this document.</p>
<h2>Misc</h2>
<p>Alongside the core components of the Names prototype system, there are a number of other components that have been developed to complement them. They are described below.</p>
<h3>Names Match Service</h3>
<p><a href="https://bitbucket.org/dan_at_mimas/names-match-service">https://bitbucket.org/dan_at_mimas/names-match-service</a></p>
<p>The Names Match Service is a RESTful web application built using the <a href="http://www.playframework.com/">Java Play Framework</a> and providing the functionality to disambiguate individuals using the Names Disambiguation package.</p>
<p>The service accepts meta data pertaining to individuals in a Json encoded list form and then uses the Names Disambiguation package to derive match scores for each of these individuals. The match scores are then returned as a Json encoded array to be used in whatever way required.</p>
<p>The service is stateless ; it does not rely upon existing Names records to perform the matching, nor does it store any meta data submitted to it, nor the outcome scores. It is therefore expected that the client will maintain the state in whatever application they are using the service for.</p>
<p>More information about API usage, response and request formats is given in the application.</p>
<h3>Names Example Data Handlers</h3>
<p>Three example data handlers are provided to demonstrate the various ways that the Names Disambigutor, Names Data Handler &amp; Names Mediator libraries can be used.</p>
<ul>
<li>
<p><strong>Names Example Data Handler</strong></p>
<p><a href="https://bitbucket.org/dan_at_mimas/names-example-data-handler">https://bitbucket.org/dan_at_mimas/names-example-data-handler</a></p>
<p>The first example data handler demonstrates how the meta data from a number of source records would be normalised using the Names Disambiguation package and then compared against each other to derive match scores.</p>
<p>In this example:</p>
<ol>
<li>Build an example data source in memory.</li>
<li>Iterate through the data source in batches, transforming each record into a normalised names record.</li>
<li>Use the names disambiguator to derive match scores for comparisons between each record.</li>
<li>Dump the resulting match scores into a tab delimited file.</li>
</ol>
</li>
<li>
<p><strong>Names Example Data Handler Two</strong></p>
<p><a href="https://bitbucket.org/dan_at_mimas/names-example-data-handler-two">https://bitbucket.org/dan_at_mimas/names-example-data-handler-two</a></p>
<p>The second example data handler shows how the names disambiguation package can be used to disambiguate the unique individuals with a source data set. It then shows how the names database manager package can be used to match against existing names records and then add / update a names database instance as necessary.</p>
<p>In this example:</p>
<ol>
<li>Build an example data source in memory.</li>
<li>Iterate through the data source in batches, transforming each record into a normalised names record.</li>
<li>Use the names disambiguator to derive match scores for comparisons between each record.</li>
<li>Merge the records that meet a particular match threshold.</li>
<li>Match against existing records in a names database instance</li>
<li>Merge any identified matches</li>
<li>Add / updated the names database instance as required.</li>
</ol>
</li>
</ul>
<h2>Appendix</h2>
<h3>A. Database Structure</h3>
<pre><code>-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 23, 2013 at 03:21 PM
-- Server version: 5.5.28
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `names_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `affiliations_summaries`
--

CREATE TABLE IF NOT EXISTS `affiliations_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `INSTITUTION_ID` int(11) DEFAULT NULL,
  `INSTITUTION_NAME` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `INSTITUTION_NAME` (`INSTITUTION_NAME`(30))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50051 ;

-- --------------------------------------------------------

--
-- Table structure for table `collaborations_summaries`
--

CREATE TABLE IF NOT EXISTS `collaborations_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  `PERSON_NAME` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `PERSON_ID` (`PERSON_ID`),
  KEY `PERSON_NAME` (`PERSON_NAME`(30))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `earlier_name_summaries`
--

CREATE TABLE IF NOT EXISTS `earlier_name_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHARS` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `CHARS` (`CHARS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `field_of_activity_summaries`
--

CREATE TABLE IF NOT EXISTS `field_of_activity_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_OF_ACTIVITY` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `FIELD_OF_ACTIVITY` (`FIELD_OF_ACTIVITY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46222 ;

-- --------------------------------------------------------

--
-- Table structure for table `homepage_summaries`
--

CREATE TABLE IF NOT EXISTS `homepage_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `identifiers_summaries`
--

CREATE TABLE IF NOT EXISTS `identifiers_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDENTIFIER` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASIS_FOR` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `IDENTIFIER` (`IDENTIFIER`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94840 ;

-- --------------------------------------------------------

--
-- Table structure for table `names_records`
--

CREATE TABLE IF NOT EXISTS `names_records` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE` tinyint(4) NOT NULL,
  `NAMES_ID` int(11) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_MODIFIED` datetime DEFAULT NULL,
  `REDIRECT` int(11) DEFAULT NULL,
  `MERGE_WITH` int(11) DEFAULT NULL,
  `DESTROY` tinyint(4) DEFAULT NULL,
  `NAME_ORDER` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `POTENTIAL_MATCHES` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `HISTORY` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NAME_ENTITIES` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `TITLES` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `FIELDS_OF_ACTIVITY` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `IDENTIFIERS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `RESULT_PUBLICATIONS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `COLLABORATIONS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AFFILIATIONS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `HOMEPAGE` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `EARLIER_NAMES` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NAME_COMPONENTS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `REDIRECT` (`REDIRECT`),
  KEY `MERGE_WITH` (`MERGE_WITH`),
  KEY `DESTROY` (`DESTROY`),
  KEY `NAME_ORDER` (`NAME_ORDER`(30)),
  KEY `NAMES_ID` (`NAMES_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50477 ;

-- --------------------------------------------------------

--
-- Table structure for table `name_component_summaries`
--

CREATE TABLE IF NOT EXISTS `name_component_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME_ID` int(11) NOT NULL,
  `CHARS` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` tinyint(4) NOT NULL,
  `POSITION` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `name_summaries`
--

CREATE TABLE IF NOT EXISTS `name_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHARS` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `USED_FROM` date NOT NULL,
  `USED_TO` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `CHARS` (`CHARS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53995 ;

-- --------------------------------------------------------

--
-- Table structure for table `result_publication_assigned_summaries`
--

CREATE TABLE IF NOT EXISTS `result_publication_assigned_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULT_PUBLICATION_ID` int(11) DEFAULT NULL,
  `IDENTIFIER` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASIS_FOR` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `IDENTIFIER` (`IDENTIFIER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `result_publication_summaries`
--

CREATE TABLE IF NOT EXISTS `result_publication_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITLE` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NUMBER_OF_RESULT_PUBLICATION` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VOLUME_OF_RESULT_PUBLICATION` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDITION` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIES` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISSUE` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_OF_PUBLICATION` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABSTRACT` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=190272 ;

-- --------------------------------------------------------

--
-- Table structure for table `title_summaries`
--

CREATE TABLE IF NOT EXISTS `title_summaries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_ID` int(11) DEFAULT NULL,
  `MATCH_SCORE` double DEFAULT NULL,
  `SOURCE_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOURCE_URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITLE` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECORD_ID` (`RECORD_ID`),
  KEY `SOURCE_NAME` (`SOURCE_NAME`),
  KEY `TITLE` (`TITLE`)
) ENGINE=InnoDB  ### A. Database StructureDEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
</code></pre>
<h3>B. Names API</h3>
<p>The following section described the Names REST API.</p>
<h4>Record Identifiers</h4>
<p>Every record within names is given a unique identifier, which also acts as a resolvable URL. Each record therefore is treated as a resource.</p>
<p>Identifiers take the following form:</p>
<p>http://{identifier provider url}/{individual OR institution}/{internal id}</p>
<p>For example within the names service environment identifiers take the following form:</p>
<ul>
<li>http://names.mimas.ac.uk/individual/1</li>
<li>http://names.mimas.ac.uk/institution/1</li>
</ul>
<h4>Search</h4>
<p>Records can be searched over using the names search API. Search queries are POSTed to the web application as follows:</p>
<p>http://names.mimas.ac.uk/search</p>
<p>The query accepts the following parameters:</p>
<ul>
<li>
<p>name </p>
<p>Description:</p>
<p>Provides search over names associated with individuals or institutions.</p>
<p>Accepts:</p>
<ul>
<li>A series of name strings, space delimited </li>
<li>A series of names strings OR delimited</li>
<li>Exact phrase enclosed in quotes</li>
<li>Wildcard operator</li>
</ul>
<p>Examples:</p>
<p>http://names.mimas.ac.uk/search?name=cox</p>
<p>http://names.mimas.ac.uk/search?name=cox b</p>
<p>http://names.mimas.ac.uk/search?name=cox or hawking</p>
<p>http://names.mimas.ac.uk/search?name="cox or hawking"</p>
<p>http://names.mimas.ac.uk/search?name=hawk*</p>
<p>http://names.mimas.ac.uk/search?name=*hawk</p>
<p>http://names.mimas.ac.uk/search?name=<em>hawk</em></p>
</li>
<li>
<p>affiliation</p>
<p>Description:</p>
<p>Provides search over names of institutions entities are associated with.</p>
<p>Accepts:</p>
<ul>
<li>A series of name strings, space delimited </li>
<li>A series of names strings OR delimited</li>
<li>Exact phrase enclosed in quotes</li>
</ul>
<p>Examples:</p>
<p>http://names.mimas.ac.uk/search?affiliation=manchester</p>
<p>http://names.mimas.ac.uk/search?affiliation=manchester or york</p>
<p>http://names.mimas.ac.uk/search?affiliation="University of Manchester"</p>
</li>
<li>
<p>fieldofactivity</p>
<p>Description:</p>
<p>Provides search over fields of activities associated with entities</p>
<p>Accepts:</p>
<ul>
<li>A series of field of activity strings, space delimited </li>
<li>A series of field of activity strings OR delimited</li>
<li>Exact phrase enclosed in quotes</li>
</ul>
<p>Examples:</p>
<p>http://names.mimas.ac.uk/search?fieldofactivity=physics</p>
<p>http://names.mimas.ac.uk/search?fieldofactivity=physics or maths</p>
<p>http://names.mimas.ac.uk/search?fieldofactivity="Electrical and Electronic Engineering"</p>
</li>
<li>
<p>identifier</p>
<p>Description:</p>
<p>Provides search over external identifiers assigned to entities</p>
<p>Accepts:</p>
<ul>
<li>A series of field of activity strings, space delimited </li>
<li>A series of field of activity strings OR delimited</li>
<li>Exact phrase enclosed in quotes</li>
</ul>
<p>Examples:</p>
<p>http://names.mimas.ac.uk/search?identifier="ISNI 0000 0001 2409 6156"</p>
<p>http://names.mimas.ac.uk/search?identifier=http://eprints.uwe.ac.uk/id/person/ext-jennifer.hill@uwe.ac.uk</p>
</li>
</ul>
<p>The parameters above can be used in conjunction:</p>
<p>http://names.mimas.ac.uk/search?name=hill j&amp;fieldofactivity="Geography and Environmental Studies"</p>
<p>http://names.mimas.ac.uk/search?name=cox b&amp;affiliation="University of Manchester"</p>
<p>http://names.mimas.ac.uk/search?fieldofactivity=physics</p>
<p>http://names.mimas.ac.uk/search?affiliation="University of Manchester"</p>
<p>In addition, to enable paging through results, the start parameter can be used:</p>
<p>http://names.mimas.ac.uk/search?name=smith&amp;start=50</p>
<h4>Output format</h4>
<p>There are three output formats available for both search results and individual records:</p>
<ul>
<li>HTML<br />
</li>
<li>Json</li>
<li>RDF+XML</li>
</ul>
<p>The client application can specify which of these formats they wish to recieve for a request by specifying it in the Accepts of the header in their HTTP request.</p></body></html>